package my.projects.egs.ServleJdbcProject.validators;

import my.projects.egs.ServleJdbcProject.dtos.request.UserRequestDto;
import my.projects.egs.ServleJdbcProject.errors.ErrorCode;
import my.projects.egs.ServleJdbcProject.errors.ErrorProvider;
import my.projects.egs.ServleJdbcProject.errors.FormError;
import my.projects.egs.ServleJdbcProject.exceptions.*;
import my.projects.egs.ServleJdbcProject.util.RegistrationConstants;
import org.apache.log4j.Logger;

import java.util.*;

public class RegistrationFormValidator {

    private static Logger logger = Logger.getLogger(RegistrationFormValidator.class.getName());
    private final ErrorProvider<FormError> formErrors;

    public RegistrationFormValidator() {
        formErrors = new ErrorProvider<>();
    }

    public void validate(UserRequestDto userRequestDto) throws FormException {
        formErrors.getErrors().clear();
        validateUsername(userRequestDto.getUsername());
        validatePassword(userRequestDto.getPassword());
        validatePasswordsMatch(userRequestDto.getPassword(), userRequestDto.getConfirmPassword());
        validateFirstName(userRequestDto.getFirstName());
        validateLastName(userRequestDto.getLastName());
        validateBirthDate(userRequestDto.getBirthDay(), userRequestDto.getBirthMonth(), userRequestDto.getBirthYear());
        if (!formErrors.getErrors().isEmpty()) {
            throw new FormException("Some forms is not valid");
        }
    }

    private void validateUsername(String username) {
        if (username.length() < RegistrationConstants.USER_VALID_SYMBOL_SIZE) {
            logger.warn("Username field is not valid");
            formErrors.addError(new FormError(ErrorCode.BAD_USERNAME.getErrorCode(), ErrorCode.BAD_USERNAME.getErrorMessage()));
        }
    }

    private void validatePassword(String password) {
        if (password.length() < RegistrationConstants.PASS_VALID_SYMBOL_SIZE) {
            logger.warn("Password field is not valid");
            formErrors.addError(new FormError(ErrorCode.BAD_PASSWORD.getErrorCode(), ErrorCode.BAD_PASSWORD.getErrorMessage()));
        }
    }

    private void validatePasswordsMatch(String password1, String password2) {
        if (!password1.equals(password2)) {
            logger.warn("Confirm password field is not valid");
            formErrors.addError(new FormError(ErrorCode.BAD_CONFIRM_PASSWORD.getErrorCode(), ErrorCode.BAD_CONFIRM_PASSWORD.getErrorMessage()));
        }
    }

    private void validateFirstName(String firstName) {
        if (firstName.isEmpty()) {
            logger.warn("First name field is not valid");
            formErrors.addError(new FormError(ErrorCode.BAD_FIRST_NAME.getErrorCode(), ErrorCode.BAD_FIRST_NAME.getErrorMessage()));
        }
    }

    private void validateLastName(String lastName) {
        if (lastName.isEmpty()) {
            logger.warn("Last name field is not valid");
            formErrors.addError(new FormError(ErrorCode.BAD_LAST_NAME.getErrorCode(), ErrorCode.BAD_LAST_NAME.getErrorMessage()));
        }
    }

    private void validateBirthDate(int birthDay, int birthMonth, int birthYear) {
        boolean isValid = true;
        List<Integer> MONTH_DAYS_31 = new ArrayList<Integer>();
        MONTH_DAYS_31.add(1);
        MONTH_DAYS_31.add(3);
        MONTH_DAYS_31.add(5);
        MONTH_DAYS_31.add(7);
        MONTH_DAYS_31.add(8);
        MONTH_DAYS_31.add(10);
        MONTH_DAYS_31.add(12);
        if (birthDay >= 1 && birthDay <= 31) {
            if ( (birthDay > 28 && birthMonth == 2) || (birthDay == 31 && !MONTH_DAYS_31.contains(birthMonth)) ) {
                logger.warn("Birth day field is not valid");
                isValid = false;
            }
        } else {
            logger.warn("Birth day field is not valid");
            isValid = false;
        }

        if (birthMonth < 1 || birthMonth > 12) {
            logger.warn("Birth month field is not valid");
            isValid = false;
        }

        if (birthYear < 1900 || birthYear > 2000) {
            logger.warn("Birth year field is not valid");
            isValid = false;
        }
        if (!isValid) {
            formErrors.addError(new FormError(ErrorCode.BAD_BIRTH_DATE.getErrorCode(), ErrorCode.BAD_BIRTH_DATE.getErrorMessage()));
        }
    }

    public Queue<FormError> getErrors() {
        return formErrors.getErrors();
    }


}
