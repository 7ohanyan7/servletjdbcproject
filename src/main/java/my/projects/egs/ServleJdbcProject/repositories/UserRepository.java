package my.projects.egs.ServleJdbcProject.repositories;


import my.projects.egs.ServleJdbcProject.entities.User;

import java.util.List;
import java.util.Optional;


public interface UserRepository {

    Optional<User> save(User user);

    Optional<User> findByUsername(String username);

    Optional<User> findById(long id);

    List<User> findAll();

    boolean delete(long id);

    Optional<User> update(User user);

}
