package my.projects.egs.ServleJdbcProject.repositories.impl;



import my.projects.egs.ServleJdbcProject.data_sources.DataSourceHikari;
import my.projects.egs.ServleJdbcProject.entities.Role;
import my.projects.egs.ServleJdbcProject.entities.User;
import my.projects.egs.ServleJdbcProject.repositories.UserRepository;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.*;

public class UserRepositoryImpl implements UserRepository {

    private static UserRepository userRepository = null;
    private static Logger logger = Logger.getLogger(UserRepositoryImpl.class.getName());

    public static UserRepository getInstance() {
        if (userRepository == null) {
            synchronized(UserRepositoryImpl.class) {
                if (userRepository == null) {
                    userRepository = new UserRepositoryImpl();
                }
            }
        }
        return userRepository;
    }

    private UserRepositoryImpl() { }

    private Connection getConnection(){
        try {
            logger.warn("trying to get connection");
            return DataSourceHikari.getConnection();
        } catch (SQLException e) {
            logger.error("cant't get connection",e);
            return null;
        }
    }

    public Optional<User> save(User user) {
        Connection connection = getConnection();
        PreparedStatement userInsertStatement = null;
        PreparedStatement roleIdSelectStatement = null;
        ResultSet generatedKeys = null;
        ResultSet selectedRoleIdResultSet = null;

        try {
            if (connection != null) {
                connection.setAutoCommit(false);
                userInsertStatement = connection.prepareStatement("insert into servletjdbc.user " +
                        "set username = ?, password = ?, firstName = ?, lastName = ?, birthDay = ?, birthMonth = ?, " +
                        "birthYear = ?", Statement.RETURN_GENERATED_KEYS);
                userInsertStatement.setString(1, user.getUsername());
                userInsertStatement.setString(2, user.getPassword());
                userInsertStatement.setString(3, user.getFirstName());
                userInsertStatement.setString(4, user.getLastName());
                userInsertStatement.setInt(5, user.getBirthDay());
                userInsertStatement.setInt(6, user.getBirthMonth());
                userInsertStatement.setInt(7, user.getBirthYear());
                userInsertStatement.execute();
                generatedKeys = userInsertStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    user.setId(generatedKeys.getLong(1));
                }
                userInsertStatement.close();
                roleIdSelectStatement = connection.prepareStatement("select id from servletjdbc.role where role = ?");
                roleIdSelectStatement.setString(1, Role.USER.name());
                selectedRoleIdResultSet = roleIdSelectStatement.executeQuery();
                if (selectedRoleIdResultSet.next()) {
                    userInsertStatement = connection.prepareStatement("insert into servletjdbc.user_role set role_id = ?, user_id = ?");
                    userInsertStatement.setInt(1, selectedRoleIdResultSet.getInt(1));
                    userInsertStatement.setLong(2, user.getId());
                    userInsertStatement.execute();
                }
                connection.commit();
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                logger.error("Exception thrown in moment rollback",ex);
            }
            logger.error("save problem", e);
        }finally {
            try {
                if (userInsertStatement != null) {
                    userInsertStatement.close();
                }
                if (roleIdSelectStatement != null) {
                    roleIdSelectStatement.close();
                }
                if (selectedRoleIdResultSet != null) {
                    selectedRoleIdResultSet.close();
                }
                if (generatedKeys != null) {
                    generatedKeys.close();
                }
                if (connection != null)
                {
                    connection.close();
                }
            } catch (SQLException e) {
                logger.error("connection closing problem",e);
            }
        }
        return Optional.ofNullable(user);
    }

    public Optional<User> findByUsername(String username) {
        Connection connection = getConnection();
        User user = null;
        PreparedStatement selectUserStatement = null;
        ResultSet selectUserResultSet = null;

        try {
            if (connection != null)
            {
                selectUserStatement = connection.prepareStatement("select * from servletjdbc.user" +
                        " where username = ?");
                selectUserStatement.setString(1, username);
                selectUserResultSet = selectUserStatement.executeQuery();
                if (selectUserResultSet.next())
                {
                    user = getUser(connection, selectUserResultSet);
                }
            }
        } catch (SQLException e) {
            logger.error("findByUsername problem", e);
        }finally {
            try {
                if (selectUserStatement != null) {
                    selectUserStatement.close();
                }
                if (selectUserResultSet != null) {
                    selectUserResultSet.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                logger.error("connection closing problem", e);
            }
        }
        return Optional.ofNullable(user);
    }

    public Optional<User> findById(long id) {
        Connection connection = getConnection();
        PreparedStatement selectUserStatement = null;
        ResultSet selectUserResultSet = null;
        User user = null;

        try {
            if (connection != null) {
                selectUserStatement = connection.prepareStatement("select * from servletjdbc.user " +
                        "where id = ?");
                selectUserStatement.setLong(1, id);
                selectUserResultSet = selectUserStatement.executeQuery();
                if (selectUserResultSet.next())
                {
                    user = getUser(connection, selectUserResultSet);
                }
            }
        } catch (SQLException e) {
            logger.error("findById problem", e);
        }finally {
            try {
                if (selectUserStatement != null) {
                    selectUserStatement.close();
                }
                if (selectUserResultSet != null) {
                    selectUserResultSet.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                logger.error("connection closing problem", e);
            }
        }
        return Optional.ofNullable(user);
    }

    public List<User> findAll() {
        Connection connection = getConnection();
        PreparedStatement usersStatement = null;
        ResultSet usersResultSet = null;
        List<User> users = new ArrayList<User>();

        try {
            if (connection != null) {
                usersStatement = connection.prepareStatement("SELECT * from servletjdbc.user");
                usersResultSet = usersStatement.executeQuery();
                while (usersResultSet.next()) {
                    User user = getUser(connection, usersResultSet);
                    users.add(user);
                }
            }
        } catch (SQLException e) {
            logger.error("findAll problem", e);
        }finally {
            try {
                if (usersStatement != null) {
                    usersStatement.close();
                }
                if (usersResultSet != null) {
                    usersResultSet.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                logger.error("connection closing problem", e);
                e.printStackTrace();
            }
        }
        return users;
    }

    public boolean delete(long id) {
        Connection connection = getConnection();
        PreparedStatement usersStatement = null;
        PreparedStatement user_roleStatement = null;
        boolean isDeleted = false;

        try {
            if (connection != null) {
                connection.setAutoCommit(false);
                usersStatement = connection.prepareStatement("delete from servletjdbc.user where id = ?");
                user_roleStatement = connection.prepareStatement("delete from servletjdbc.user_role where user_id = ?");
                usersStatement.setLong(1, id);
                user_roleStatement.setLong(1, id);
                user_roleStatement.execute();
                usersStatement.execute();
                connection.commit();
                isDeleted = true;
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                logger.error("Exception thrown in moment rollback",e);
            }
            logger.error("delete problem",e);
        }finally {
            try {
                if (user_roleStatement != null) {
                    user_roleStatement.close();
                }
                if (usersStatement != null) {
                    usersStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                logger.error("Connection closing problem");
            }
        }
        return isDeleted;
    }

    public Optional<User> update(User user) {
        Connection connection = getConnection();
        PreparedStatement updateUserStatement = null;
        try {
            if (connection != null) {
                connection.setAutoCommit(false);
                updateUserStatement = connection.prepareStatement("update servletjdbc.user set username = ?, " +
                        "password = ?, firstName = ?, lastName = ?, birthDay = ?, birthMonth = ?, birthYear = ? where id = ?");
                updateUserStatement.setString(1, user.getUsername());
                updateUserStatement.setString(2, user.getPassword());
                updateUserStatement.setString(3, user.getFirstName());
                updateUserStatement.setString(4, user.getLastName());
                updateUserStatement.setInt(5, user.getBirthDay());
                updateUserStatement.setInt(6, user.getBirthMonth());
                updateUserStatement.setInt(7, user.getBirthYear());
                updateUserStatement.setLong(8, user.getId());
                updateUserStatement.executeUpdate();
                connection.commit();
            }
        }  catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                logger.error("Exception thrown in moment rollback");
            }
            e.printStackTrace();
        }finally {
            try {
                if (updateUserStatement != null) {
                    updateUserStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                logger.error("Connection closing problem");
            }
        }
        return Optional.ofNullable(user);
    }

    private Set<Role> getRoles(Connection connection, long id) throws SQLException {

        PreparedStatement role_idStatement = connection.prepareStatement("select role_id from servletjdbc.user_role where user_id = ?");
        role_idStatement.setLong(1, id);
        ResultSet role_idResultSet = role_idStatement.executeQuery();
        Set<Role> roles = new HashSet<Role>();
        PreparedStatement rolesStatement = null;
        ResultSet rolesResultSet = null;
        while (role_idResultSet.next()) {
            rolesStatement = connection.prepareStatement("SELECT role from servletjdbc.role where id = ?");
            rolesStatement.setLong(1, role_idResultSet.getLong(1));
            rolesResultSet = rolesStatement.executeQuery();
            while (rolesResultSet.next()) {
                roles.add(Role.valueOf(rolesResultSet.getString(1)));
            }
        }

        role_idStatement.close();
        role_idResultSet.close();
        if (rolesStatement != null) {
            rolesStatement.close();
        }
        if (rolesResultSet != null) {
            rolesResultSet.close();
        }

        return roles;
    }

    private User getUser(Connection connection, ResultSet userResultSet) throws SQLException {
        long id = userResultSet.getLong(1);
        String username = userResultSet.getString(2);
        String password = userResultSet.getString(3);
        String firstName = userResultSet.getString(4);
        String lastName = userResultSet.getString(5);
        int birthDay = userResultSet.getInt(6);
        int birthMonth = userResultSet.getInt(7);
        int birthYear = userResultSet.getInt(8);
        Set<Role> roles = getRoles(connection, id);
        User user = User.builder()
                .id(id)
                .username(username)
                .password(password)
                .firstName(firstName)
                .lastName(lastName)
                .birthDay(birthDay)
                .birthMonth(birthMonth)
                .birthYear(birthYear)
                .roles(roles)
                .build();
        return user;
    }
}
