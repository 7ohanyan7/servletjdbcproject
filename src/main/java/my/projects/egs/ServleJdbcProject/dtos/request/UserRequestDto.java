package my.projects.egs.ServleJdbcProject.dtos.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import my.projects.egs.ServleJdbcProject.entities.Role;

import java.util.Set;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class UserRequestDto {

    private long id;

    private String username;

    private String password;

    private String confirmPassword;

    private String firstName;

    private String lastName;

    private int birthDay;

    private int birthMonth;

    private int birthYear;

    private Set<Role> roles;

}
