package my.projects.egs.ServleJdbcProject.dtos.response;

import lombok.Data;
import lombok.NoArgsConstructor;
import my.projects.egs.ServleJdbcProject.entities.Role;

import java.util.Set;

@Data
@NoArgsConstructor
public class UserResponseDto {

    private long id;

    private String username;

    private String firstName;

    private String lastName;

    private int birthDay;

    private int birthMonth;

    private int birthYear;

    private Set<Role> roles;

}
