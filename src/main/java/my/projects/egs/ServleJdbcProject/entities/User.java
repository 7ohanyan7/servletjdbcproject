package my.projects.egs.ServleJdbcProject.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {

    private long id;

    private String username;

    private String password;

    private String firstName;

    private String lastName;

    private int birthDay;

    private int birthMonth;

    private int birthYear;

    private Set<Role> roles;

}
