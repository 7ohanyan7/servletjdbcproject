package my.projects.egs.ServleJdbcProject.exceptions;

public class UsernameNotFoundException extends LoginException {
    public UsernameNotFoundException(String message) {
        super(message);
    }
}
