package my.projects.egs.ServleJdbcProject.exceptions;

public class LoginException extends Exception {
    public LoginException(String message) {
        super(message);
    }
}
