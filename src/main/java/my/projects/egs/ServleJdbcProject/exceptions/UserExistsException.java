package my.projects.egs.ServleJdbcProject.exceptions;

public class UserExistsException extends RegistrationException {
    public UserExistsException(String message) {
        super(message);
    }
}
