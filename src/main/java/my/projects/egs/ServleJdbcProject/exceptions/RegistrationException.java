package my.projects.egs.ServleJdbcProject.exceptions;

public class RegistrationException extends Exception {
    public RegistrationException(String message) {
        super(message);
    }
}
