package my.projects.egs.ServleJdbcProject.exceptions;

public class FormException extends RegistrationException {
    public FormException(String message) {
        super(message);
    }
}
