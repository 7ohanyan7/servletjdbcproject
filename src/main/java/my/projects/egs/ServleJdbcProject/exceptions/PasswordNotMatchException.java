package my.projects.egs.ServleJdbcProject.exceptions;

public class PasswordNotMatchException extends LoginException {
    public PasswordNotMatchException(String message) {
        super(message);
    }
}
