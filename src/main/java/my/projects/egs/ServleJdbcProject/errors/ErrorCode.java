package my.projects.egs.ServleJdbcProject.errors;

import my.projects.egs.ServleJdbcProject.validators.RegistrationFormValidator;

public enum ErrorCode {
    BAD_USERNAME(400_01, "Username must be large than " + RegistrationFormValidator.USER_VALID_SYMBOL_SIZE),
    BAD_PASSWORD(400_02, "Password must be large than " + RegistrationFormValidator.PASS_VALID_SYMBOL_SIZE),
    BAD_CONFIRM_PASSWORD(400_03, "Passwords not match"),
    BAD_FIRST_NAME(400_04, "First name must be filled"),
    BAD_LAST_NAME(400_05, "Last name must be filled"),
    BAD_BIRTH_DATE(400_06, "Birth date is not valid");

    private int errorCode;
    private String errorMessage;

    ErrorCode(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
