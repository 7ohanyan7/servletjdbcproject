package my.projects.egs.ServleJdbcProject.errors;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class ErrorProvider<ErrorType extends Error> {

    private Queue<ErrorType> errors;


    public ErrorProvider() {
        errors = new ArrayDeque<>();
    }

    public void addError(ErrorType error) {
        errors.add(error);
    }

    public Queue<ErrorType> getErrors() {
        return errors;
    }
}
