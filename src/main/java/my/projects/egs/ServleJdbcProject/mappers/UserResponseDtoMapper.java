package my.projects.egs.ServleJdbcProject.mappers;

import my.projects.egs.ServleJdbcProject.dtos.response.UserResponseDto;
import my.projects.egs.ServleJdbcProject.entities.User;

import java.util.LinkedList;
import java.util.List;

public class UserResponseDtoMapper {

    private static UserResponseDtoMapper userResponseDtoMapper;

    public static UserResponseDtoMapper getInstance() {
        if (userResponseDtoMapper == null) {
            synchronized(UserResponseDtoMapper.class) {
                if (userResponseDtoMapper == null) {
                    userResponseDtoMapper = new UserResponseDtoMapper();
                }
            }
        }
        return userResponseDtoMapper;
    }

    private UserResponseDtoMapper(){}

    public UserResponseDto userToUserResponseDto(User user){
        UserResponseDto userResponseDto = null;
        if (user != null)
        {
            userResponseDto = new UserResponseDto();
            userResponseDto.setId(user.getId());
            userResponseDto.setUsername(user.getUsername());
            userResponseDto.setFirstName(user.getFirstName());
            userResponseDto.setLastName(user.getLastName());
            userResponseDto.setBirthDay(user.getBirthDay());
            userResponseDto.setBirthMonth(user.getBirthMonth());
            userResponseDto.setBirthYear(user.getBirthYear());
            userResponseDto.setRoles(user.getRoles());
        }

        return userResponseDto;
    }

    public List<UserResponseDto> usersToUserResponseDtos(List<User> users){
        List<UserResponseDto> userResponseDtos = new LinkedList<UserResponseDto>();

        for (User user : users) {
            userResponseDtos.add(userToUserResponseDto(user));
        }

        return userResponseDtos;
    }

    public User userResponseDtoToUser(UserResponseDto userResponseDto){
        User user = null;
        if (userResponseDto != null)
        {
            user = new User();
            user.setId(userResponseDto.getId());
            user.setUsername(userResponseDto.getUsername());
            user.setFirstName(userResponseDto.getFirstName());
            user.setLastName(userResponseDto.getLastName());
            user.setBirthDay(userResponseDto.getBirthDay());
            user.setBirthMonth(userResponseDto.getBirthMonth());
            user.setBirthYear(userResponseDto.getBirthYear());
            user.setRoles(userResponseDto.getRoles());
        }

        return user;
    }

    public List<User> userResponseDtosToUsers(List<UserResponseDto> userResponseDtos){
        List<User> users = new LinkedList<User>();

        for (UserResponseDto userResponseDto : userResponseDtos) {
            users.add(userResponseDtoToUser(userResponseDto));
        }

        return users;
    }
}
