package my.projects.egs.ServleJdbcProject.mappers;

import my.projects.egs.ServleJdbcProject.dtos.request.UserRequestDto;
import my.projects.egs.ServleJdbcProject.dtos.request.UsernamePasswordDto;
import my.projects.egs.ServleJdbcProject.entities.User;

import java.util.LinkedList;
import java.util.List;

public class UsernamePasswordDtoMapper {

    private static UsernamePasswordDtoMapper usernamePasswordDtoMapper = null;

    public static UsernamePasswordDtoMapper getInstance() {
        if (usernamePasswordDtoMapper == null) {
            synchronized(UsernamePasswordDtoMapper.class) {
                if (usernamePasswordDtoMapper == null) {
                    usernamePasswordDtoMapper = new UsernamePasswordDtoMapper();
                }
            }
        }
        return usernamePasswordDtoMapper;
    }

    private UsernamePasswordDtoMapper(){}

    public UsernamePasswordDto userToUsernamePasswordDto(User user){
        UsernamePasswordDto usernamePasswordDto = null;
        if (user != null)
        {
            usernamePasswordDto = new UsernamePasswordDto();
            usernamePasswordDto.setUsername(user.getUsername());
            usernamePasswordDto.setPassword(user.getPassword());
        }

        return usernamePasswordDto;
    }

    public List<UsernamePasswordDto> usersToUsernamePasswordDtos(List<User> users){
        List<UsernamePasswordDto> usernamePasswordDtos = new LinkedList<UsernamePasswordDto>();

        for (User user :
                users) {
            usernamePasswordDtos.add(userToUsernamePasswordDto(user));
        }

        return usernamePasswordDtos;
    }

    public User usernamePasswordDtoToUser(UsernamePasswordDto usernamePasswordDto){
        User user = null;
        if (usernamePasswordDto != null)
        {
            user = new User();
            user.setUsername(usernamePasswordDto.getUsername());
            user.setPassword(usernamePasswordDto.getPassword());
        }

        return user;
    }

    public List<User> usernamePasswordDtosToUsers(List<UsernamePasswordDto> usernamePasswordDtos){
        List<User> users = new LinkedList<User>();

        for (UsernamePasswordDto usernamePasswordDto :
                usernamePasswordDtos) {
            users.add(usernamePasswordDtoToUser(usernamePasswordDto));
        }

        return users;
    }

}
