package my.projects.egs.ServleJdbcProject.mappers;

import my.projects.egs.ServleJdbcProject.dtos.request.UserRequestDto;
import my.projects.egs.ServleJdbcProject.entities.User;

import java.util.LinkedList;
import java.util.List;

public class UserRequestDtoMapper {

    private static UserRequestDtoMapper userRequestDtoMapper = null;

    public static UserRequestDtoMapper getInstance() {
        if (userRequestDtoMapper == null) {
            synchronized(UserRequestDtoMapper.class) {
                if (userRequestDtoMapper == null) {
                    userRequestDtoMapper = new UserRequestDtoMapper();
                }
            }
        }
        return userRequestDtoMapper;
    }

    private UserRequestDtoMapper(){}

    public UserRequestDto userToUserRequestDto(User user){
        UserRequestDto userRequestDto = null;
        if (user != null)
        {
            userRequestDto = new UserRequestDto();
            userRequestDto.setId(user.getId());
            userRequestDto.setUsername(user.getUsername());
            userRequestDto.setPassword(user.getPassword());
            userRequestDto.setFirstName(user.getFirstName());
            userRequestDto.setLastName(user.getLastName());
            userRequestDto.setBirthDay(user.getBirthDay());
            userRequestDto.setBirthMonth(user.getBirthMonth());
            userRequestDto.setBirthYear(user.getBirthYear());
            userRequestDto.setRoles(user.getRoles());
        }

        return userRequestDto;
    }

    public List<UserRequestDto> usersToUserRequestDtos(List<User> users){
        List<UserRequestDto> userRequestDtos = new LinkedList<UserRequestDto>();

        for (User user :
                users) {
            userRequestDtos.add(userToUserRequestDto(user));
        }

        return userRequestDtos;
    }

    public User userRequestDtoToUser(UserRequestDto userRequestDto){
        User user = null;
        if (userRequestDto != null)
        {
            user = new User();
            user.setId(userRequestDto.getId());
            user.setUsername(userRequestDto.getUsername());
            user.setPassword(userRequestDto.getPassword());
            user.setFirstName(userRequestDto.getFirstName());
            user.setLastName(userRequestDto.getLastName());
            user.setBirthDay(userRequestDto.getBirthDay());
            user.setBirthMonth(userRequestDto.getBirthMonth());
            user.setBirthYear(userRequestDto.getBirthYear());
            user.setRoles(userRequestDto.getRoles());
        }

        return user;
    }

    public List<User> userRequestDtosToUsers(List<UserRequestDto> userRequestDtos){
        List<User> users = new LinkedList<User>();

        for (UserRequestDto userRequestDto :
                userRequestDtos) {
            users.add(userRequestDtoToUser(userRequestDto));
        }

        return users;
    }
}
