package my.projects.egs.ServleJdbcProject.data_sources;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

public class DataSourceHikari {

    private static Logger logger = Logger.getLogger(DataSourceHikari.class.getName());
    private static HikariDataSource hikariDataSource = null;

    private DataSourceHikari() {
    }

    private static void getDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName("com.mysql.jdbc.Driver");
        hikariConfig.setJdbcUrl("jdbc:mysql://localhost/servletjdbc?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
        hikariConfig.setUsername("root");
        hikariConfig.setPassword("root");
        hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
        hikariConfig.addDataSourceProperty( "prepStmtCacheSize" , "250" );
        hikariConfig.addDataSourceProperty( "prepStmtCacheSqlLimit" , "2048" );
        logger.info("Hikari configs accepted");
        hikariDataSource = new HikariDataSource(hikariConfig);
    }

    public static Connection getConnection() throws SQLException {
        if (hikariDataSource == null) {
            synchronized (DataSourceHikari.class) {
                if (hikariDataSource == null) {
                    getDataSource();
                }
            }
        }
        return hikariDataSource.getConnection();
    }

}
