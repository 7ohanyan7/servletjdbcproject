package my.projects.egs.ServleJdbcProject.filters;

import my.projects.egs.ServleJdbcProject.dtos.response.UserResponseDto;
import my.projects.egs.ServleJdbcProject.entities.Role;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AdminFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse)servletResponse;

        HttpSession session = request.getSession(false);
        UserResponseDto userResponseDto = (UserResponseDto) session.getAttribute("principal") ;

        if (userResponseDto != null && userResponseDto.getRoles().contains(Role.ADMIN)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            request.setAttribute("accessDenied", "You have not permission");
            request.getRequestDispatcher("/welcome").forward(request, response);
        }

    }
}
