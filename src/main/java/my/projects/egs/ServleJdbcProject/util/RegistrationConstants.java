package my.projects.egs.ServleJdbcProject.util;

public interface RegistrationConstants {
    int USER_VALID_SYMBOL_SIZE = 3;
    int PASS_VALID_SYMBOL_SIZE = 8;
}
