package my.projects.egs.ServleJdbcProject.servlets;

import my.projects.egs.ServleJdbcProject.dtos.request.UserRequestDto;
import my.projects.egs.ServleJdbcProject.services.UserService;
import my.projects.egs.ServleJdbcProject.services.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UpdateUser extends HttpServlet {

    private UserService userService;


    @Override
    public void init() {
        userService = UserServiceImpl.getInstance();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (req.getParameter("id") != null) {
            long id = Long.parseLong(req.getParameter("id"));
            UserRequestDto userRequestDto = userService.findById(id).get();
            if (!req.getParameter("password").equals("")) {
                userRequestDto.setPassword(req.getParameter("password"));
            }
            userRequestDto.setUsername(req.getParameter("username"));
            userRequestDto.setFirstName(req.getParameter("firstName"));
            userRequestDto.setLastName(req.getParameter("lastName"));
            userService.update(userRequestDto);
        }
        resp.sendRedirect("/users");
    }
}
