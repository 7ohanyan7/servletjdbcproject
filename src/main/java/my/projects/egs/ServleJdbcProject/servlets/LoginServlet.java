package my.projects.egs.ServleJdbcProject.servlets;

import my.projects.egs.ServleJdbcProject.dtos.request.UserRequestDto;
import my.projects.egs.ServleJdbcProject.dtos.request.UsernamePasswordDto;
import my.projects.egs.ServleJdbcProject.dtos.response.UserResponseDto;
import my.projects.egs.ServleJdbcProject.exceptions.LoginException;
import my.projects.egs.ServleJdbcProject.services.UserService;
import my.projects.egs.ServleJdbcProject.services.impl.UserServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginServlet extends HttpServlet {

    private UserService userService;

    @Override
    public void init() {
        userService = UserServiceImpl.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("login.jsp");
        requestDispatcher.include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        UsernamePasswordDto usernamePasswordDto = new UsernamePasswordDto();
        usernamePasswordDto.setUsername(req.getParameter("username"));
        usernamePasswordDto.setPassword(req.getParameter("password"));

        try {
            UserResponseDto userResponseDto = userService.login(usernamePasswordDto);
            HttpSession session = req.getSession();
            session.setAttribute("principal", userResponseDto);
            resp.sendRedirect("/welcome");
        } catch (LoginException e) {
            req.setAttribute("errorMessage", e.getMessage());
            req.getRequestDispatcher("login.jsp").include(req, resp);
            e.printStackTrace();
        }

    }
}
