package my.projects.egs.ServleJdbcProject.servlets;

import my.projects.egs.ServleJdbcProject.dtos.response.UserResponseDto;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogoutServlet extends HttpServlet {

    private static Logger logger = Logger.getLogger(LogoutServlet.class.getName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        logger.info( ((UserResponseDto)req.getSession().getAttribute("principal")).getUsername() + " has logged out");
        req.getSession().invalidate();
        resp.sendRedirect("/login");
    }
}
