package my.projects.egs.ServleJdbcProject.servlets;

import my.projects.egs.ServleJdbcProject.dtos.response.UserResponseDto;
import my.projects.egs.ServleJdbcProject.mappers.UserRequestDtoMapper;
import my.projects.egs.ServleJdbcProject.mappers.UserResponseDtoMapper;
import my.projects.egs.ServleJdbcProject.services.UserService;
import my.projects.egs.ServleJdbcProject.services.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class UsersServlet extends HttpServlet {

    private UserService userService;
    private UserResponseDtoMapper userResponseDtoMapper;
    private UserRequestDtoMapper userRequestDtoMapper;

    @Override
    public void init() {
        userService = UserServiceImpl.getInstance();
        userRequestDtoMapper = UserRequestDtoMapper.getInstance();
        userResponseDtoMapper = UserResponseDtoMapper.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<UserResponseDto> users = userResponseDtoMapper.usersToUserResponseDtos(
                userRequestDtoMapper.userRequestDtosToUsers(userService.findAll()));
        req.setAttribute("users", users);
        req.getRequestDispatcher("users.jsp").include(req, resp);

    }
}
