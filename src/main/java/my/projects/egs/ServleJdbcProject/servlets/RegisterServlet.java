package my.projects.egs.ServleJdbcProject.servlets;

import com.sun.jmx.remote.internal.ArrayQueue;
import my.projects.egs.ServleJdbcProject.errors.ErrorCode;
import my.projects.egs.ServleJdbcProject.forms.registration.Field;
import my.projects.egs.ServleJdbcProject.dtos.request.UserRequestDto;
import my.projects.egs.ServleJdbcProject.exceptions.FormException;
import my.projects.egs.ServleJdbcProject.exceptions.RegistrationException;
import my.projects.egs.ServleJdbcProject.forms.registration.RegistrationForm;
import my.projects.egs.ServleJdbcProject.services.UserService;
import my.projects.egs.ServleJdbcProject.services.impl.UserServiceImpl;
import my.projects.egs.ServleJdbcProject.validators.RegistrationFormValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class RegisterServlet extends HttpServlet {

    private UserService userService;
    private RegistrationFormValidator registrationFormValidator;
    private List<Field> form;

    @Override
    public void init() {
        userService = UserServiceImpl.getInstance();
        registrationFormValidator = new RegistrationFormValidator();
        form = new ArrayList<>();
        for (int i = 0;i < RegistrationForm.values().length;i++) {
            form.add(new Field(ErrorCode.values()[i].getErrorCode(), RegistrationForm.values()[i].getFieldName(), RegistrationForm.values()[i].getFieldType()));
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");


        req.setAttribute("formFields", form);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("register.jsp");
        requestDispatcher.include(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        addFieldValuesToForm(new UserRequestDto(), form);
        req.setAttribute("formFields", form);
        String date = req.getParameter(RegistrationForm.BIRTH_DATE.getFieldName());
        int day = 0;
        int month = 0;
        int year = 0;
        if (date.split("-").length == 3) {
            String[] dateArr = date.split("-");
            day = Integer.parseInt(dateArr[2]);
            month = Integer.parseInt(dateArr[1]);
            year = Integer.parseInt(dateArr[0]);
        }

        UserRequestDto userRequestDto = UserRequestDto.builder()
                .username(req.getParameter(RegistrationForm.USERNAME.getFieldName()))
                .password(req.getParameter(RegistrationForm.PASSWORD.getFieldName()))
                .confirmPassword(req.getParameter(RegistrationForm.CONFIRM_PASSWORD.getFieldName()))
                .firstName(req.getParameter(RegistrationForm.FIRST_NAME.getFieldName()))
                .lastName(req.getParameter(RegistrationForm.LAST_NAME.getFieldName()))
                .birthDay(day)
                .birthMonth(month)
                .birthYear(year)
                .build();
        try {
            registrationFormValidator.validate(userRequestDto);
            userService.register(userRequestDto);
            req.setAttribute("successReg", "Successfully registered");
            req.getRequestDispatcher("login.jsp").include(req, resp);
        } catch (FormException e) {
            addFieldValuesToForm(userRequestDto, form);
            req.setAttribute("formErrors", registrationFormValidator.getErrors());
            req.getRequestDispatcher("register.jsp").include(req, resp);
        } catch (RegistrationException e) {
            addFieldValuesToForm(userRequestDto, form);
            req.setAttribute("errorMessage", e.getMessage());
            req.getRequestDispatcher("register.jsp").include(req, resp);
        }

    }

    private void addFieldValuesToForm(UserRequestDto userRequestDto, List<Field> form) {
        form.get(0).setFieldValue(userRequestDto.getUsername());
        form.get(1).setFieldValue(userRequestDto.getPassword());
        form.get(2).setFieldValue(userRequestDto.getConfirmPassword());
        form.get(3).setFieldValue(userRequestDto.getFirstName());
        form.get(4).setFieldValue(userRequestDto.getLastName());
        form.get(5).setFieldValue(userRequestDto.getBirthYear() + "-" +
                userRequestDto.getBirthMonth() + "-" + userRequestDto.getBirthDay());
    }
}
