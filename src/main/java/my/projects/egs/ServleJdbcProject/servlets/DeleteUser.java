package my.projects.egs.ServleJdbcProject.servlets;

import my.projects.egs.ServleJdbcProject.services.UserService;
import my.projects.egs.ServleJdbcProject.services.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteUser extends HttpServlet {

    private UserService userService;

    @Override
    public void init() {
        userService = UserServiceImpl.getInstance();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        if (req.getParameter("id") != null) {
            long id = Long.parseLong(req.getParameter("id"));
            userService.delete(id);
        }
        resp.sendRedirect("/users");
    }

}
