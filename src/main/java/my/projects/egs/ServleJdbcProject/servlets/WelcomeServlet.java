package my.projects.egs.ServleJdbcProject.servlets;

import my.projects.egs.ServleJdbcProject.dtos.response.UserResponseDto;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class WelcomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        req.setAttribute("username", ( (UserResponseDto) req.getSession().getAttribute("principal") ).getUsername());
        req.setAttribute("firstName", ( (UserResponseDto) req.getSession().getAttribute("principal") ).getFirstName());
        req.setAttribute("lastName", ( (UserResponseDto) req.getSession().getAttribute("principal") ).getLastName());
        req.setAttribute("birthDay", ( (UserResponseDto) req.getSession().getAttribute("principal") ).getBirthDay());
        req.setAttribute("birthMonth", ( (UserResponseDto) req.getSession().getAttribute("principal") ).getBirthMonth());
        req.setAttribute("birthYear", ( (UserResponseDto) req.getSession().getAttribute("principal") ).getBirthYear());
        req.getRequestDispatcher("welcome.jsp").include(req, resp);
    }


}
