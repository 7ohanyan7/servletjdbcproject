package my.projects.egs.ServleJdbcProject.services;

import my.projects.egs.ServleJdbcProject.dtos.request.UserRequestDto;
import my.projects.egs.ServleJdbcProject.dtos.request.UsernamePasswordDto;
import my.projects.egs.ServleJdbcProject.dtos.response.UserResponseDto;
import my.projects.egs.ServleJdbcProject.exceptions.LoginException;
import my.projects.egs.ServleJdbcProject.exceptions.RegistrationException;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Optional<UserRequestDto> save(UserRequestDto userRequestDto);

    Optional<UserRequestDto> findByUsername(String username);

    Optional<UserRequestDto> findById(long id);

    List<UserRequestDto> findAll();

    boolean delete(long id);

    Optional<UserRequestDto> update(UserRequestDto userRequestDto);

    void register(UserRequestDto userRequestDto) throws RegistrationException;

    UserResponseDto login(UsernamePasswordDto usernamePasswordDto) throws LoginException;

}
