package my.projects.egs.ServleJdbcProject.services.impl;

import my.projects.egs.ServleJdbcProject.dtos.request.UserRequestDto;
import my.projects.egs.ServleJdbcProject.dtos.request.UsernamePasswordDto;
import my.projects.egs.ServleJdbcProject.dtos.response.UserResponseDto;
import my.projects.egs.ServleJdbcProject.entities.Role;
import my.projects.egs.ServleJdbcProject.entities.User;
import my.projects.egs.ServleJdbcProject.exceptions.*;
import my.projects.egs.ServleJdbcProject.mappers.UserRequestDtoMapper;
import my.projects.egs.ServleJdbcProject.mappers.UserResponseDtoMapper;
import my.projects.egs.ServleJdbcProject.repositories.UserRepository;
import my.projects.egs.ServleJdbcProject.repositories.impl.UserRepositoryImpl;
import my.projects.egs.ServleJdbcProject.services.UserService;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements UserService {

    private static UserService userService = null;
    private static Logger logger = Logger.getLogger(UserServiceImpl.class.getName());
    private UserRequestDtoMapper userRequestDtoMapper;
    private UserResponseDtoMapper userResponseDtoMapper;
    private UserRepository userRepository;

    public static UserService getInstance() {
        if (userService == null) {
            synchronized(UserServiceImpl.class) {
                if (userService == null) {
                    userService = new UserServiceImpl();
                }
            }
        }
        return userService;
    }

    private UserServiceImpl() {
        userRequestDtoMapper = UserRequestDtoMapper.getInstance();
        userResponseDtoMapper = UserResponseDtoMapper.getInstance();
        userRepository = UserRepositoryImpl.getInstance();
    }

    public Optional<UserRequestDto> save(UserRequestDto userRequestDto) {
        Optional<User> userOptional = userRepository.save(userRequestDtoMapper.userRequestDtoToUser(userRequestDto));
        return userOptional.map(user -> userRequestDtoMapper.userToUserRequestDto(user));
    }

    public Optional<UserRequestDto> findByUsername(String username) {
        Optional<User> userOptional = userRepository.findByUsername(username);
        return userOptional.map(user -> userRequestDtoMapper.userToUserRequestDto(user));
    }

    public Optional<UserRequestDto> findById(long id) {
        Optional<User> userOptional = userRepository.findById(id);
        return userOptional.map(user -> userRequestDtoMapper.userToUserRequestDto(user));
    }

    public List<UserRequestDto> findAll() {
        return userRequestDtoMapper.usersToUserRequestDtos(userRepository.findAll());
    }

    public boolean delete(long id) {
        return userRepository.delete(id);
    }

    public Optional<UserRequestDto> update(UserRequestDto userRequestDto) {
        Optional<User> userOptional = userRepository.update(userRequestDtoMapper.userRequestDtoToUser(userRequestDto));
        return userOptional.map(user -> userRequestDtoMapper.userToUserRequestDto(user));
    }

    public void register(UserRequestDto userRequestDto) throws RegistrationException {
        if (!findByUsername(userRequestDto.getUsername()).isPresent())
        {
            logger.info("user with " + userRequestDto.getUsername() + " username has registered");
            userRequestDto.setRoles(Collections.singleton(Role.USER));
            save(userRequestDto);
        } else {
            logger.info("user with " + userRequestDto.getUsername() + " username tried to register");
            throw new UserExistsException("User already exists");
        }
    }

    public UserResponseDto login(UsernamePasswordDto usernamePasswordDto) throws LoginException {
        logger.info("user with " + usernamePasswordDto.getUsername() + " username tried to login");
        Optional<UserRequestDto> userFromDB = findByUsername(usernamePasswordDto.getUsername());
        UserRequestDto userRequestDto = userFromDB.orElseThrow(
                () -> new UsernameNotFoundException("Username not found"));
        if (userRequestDto.getPassword().equals(usernamePasswordDto.getPassword())) {
            logger.info("user with " + usernamePasswordDto.getUsername() + " has logged in");
            return userResponseDtoMapper.userToUserResponseDto(userRequestDtoMapper.userRequestDtoToUser(userRequestDto));
        } else {
            logger.info("user with " + usernamePasswordDto.getUsername() + " username tried to login with invalid password");
            throw new PasswordNotMatchException("Password not match");
        }

    }

}
