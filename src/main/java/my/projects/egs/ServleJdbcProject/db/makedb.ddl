CREATE DATABASE servletjdbc;
use servletjdbc;

create table role
(
  id   int         not null
    primary key,
  role varchar(20) not null,
  constraint role_role_uindex
    unique (role)
);

create table user
(
  id         bigint auto_increment
    primary key,
  username   varchar(255) not null,
  password   varchar(255) not null,
  firstName  varchar(255) not null,
  lastName   varchar(255) not null,
  birthDay   int          not null,
  birthMonth int          not null,
  birthYear  int          not null,
  constraint users_username_uindex
    unique (username)
);

create table user_role
(
  user_id bigint not null,
  role_id int    not null,
  constraint user_role_role_id_fk
    foreign key (role_id) references role (id),
  constraint user_role_users_id_fk
    foreign key (user_id) references user (id)
);

