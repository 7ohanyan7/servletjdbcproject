package my.projects.egs.ServleJdbcProject.forms.registration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class Field {
    private int errorCode;
    private String fieldName;
    private String fieldValue;
    private String fieldType;
    private boolean hasError;

    public Field(int errorCode, String fieldName, String fieldType) {
        this.errorCode = errorCode;
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.fieldValue = "";
        this.hasError = false;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }
}
