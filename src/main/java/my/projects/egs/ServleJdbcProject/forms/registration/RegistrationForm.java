package my.projects.egs.ServleJdbcProject.forms.registration;

public enum  RegistrationForm {
    USERNAME("Username : ", "text"),
    PASSWORD("Password : ", "password"),
    CONFIRM_PASSWORD("Confirm password : ", "password"),
    FIRST_NAME("First name : ", "text"),
    LAST_NAME("Last name : ", "text"),
    BIRTH_DATE("Birth date : ", "date");

    private String fieldName;
    private String fieldType;

    RegistrationForm(String fieldName, String fieldType) {
        this.fieldName = fieldName;
        this.fieldType = fieldType;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }
}
