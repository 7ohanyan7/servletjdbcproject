<%@ page import="my.projects.egs.ServleJdbcProject.dtos.response.UserResponseDto" %><%--
  Created by IntelliJ IDEA.
  User: hranto
  Date: 10/11/2019
  Time: 6:06 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome</title>
</head>
<body>

    Hello ${firstName} ${lastName}
    <br>
    Your username is ${username}
    <br>
    Birth date - ${birthDay}/${birthMonth}/${birthYear}
    <form action="/users" method="get">
        <input type="submit" value="Users">
    </form>
    <form action="/logout" method="get">
        <input type="submit" value="Logout">
    </form>
    <p style="color:red;">${accessDenied}</p>


</body>
</html>
