
<%--
  Created by IntelliJ IDEA.
  User: hranto
  Date: 10/11/2019
  Time: 4:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<div class="form">
    <form action="/register" method="post" style="float: left">
        <table>
            <c:forEach var="field" items="${formFields}">
                <tr style="height: 30px">
                    <td>
                            ${field.getFieldName()}
                    </td>
                    <td>
                        <input type="${field.getFieldType()}" name="${field.getFieldName()}"
                               value="${field.getFieldValue()}"/>
                    </td>
                    <c:if test="${formErrors.peek().getErrorCode() == field.getErrorCode()}">
                        <td style="color: red;">
                                ${formErrors.poll().getErrorMessage()}
                        </td>
                    </c:if>
                </tr>
            </c:forEach>
        </table>
        <p style="color: red">${errorMessage}</p>
        <input type="submit" value="Register">
        <a href="/login">Already registered?</a>
    </form>
</div>
</body>
</html>