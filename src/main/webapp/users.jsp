<%--
  Created by IntelliJ IDEA.
  User: hranto
  Date: 10/11/2019
  Time: 6:59 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
</head>
<body>
<table style="width:100%;border-collapse: collapse">
    <tr style="border-collapse: collapse">
        <th>Username</th>
        <th>Password</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Actions</th>
    </tr>
    <c:forEach var="user" items="${users}">
        <tr style="border-collapse: collapse">
            <form action="${pageContext.request.contextPath}/users/update" method="post">
                <input hidden value="${user.id}" name="id"/>
                <td><input type="text" value="${user.username}" name="username"/></td>
                <td><input type="password" name="password"/></td>
                <td><input type="text" value="${user.firstName}" name="firstName"/></td>
                <td><input type="text" value="${user.lastName}" name="lastName"/></td>
                <td><input type="submit" value="Update"/></td>
            </form>
            <form action="${pageContext.request.contextPath}/users/delete" method="post">
                <td>
                    <input hidden value="${user.id}" name="id"/>
                    <input type="submit" value="Delete"/>
                </td>
            </form>
        </tr>
    </c:forEach>
</table>
</body>
</html>