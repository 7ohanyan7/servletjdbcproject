<%--
  Created by IntelliJ IDEA.
  User: hranto
  Date: 10/11/2019
  Time: 4:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<div>
    <p style="color: red">${noAuth}</p>
    <p style="color: green">${successReg}</p>
    <form action="/login" method="post">
        <table>
            <tr>
                <td>
                    Username:
                </td>
                <td>
                    <input type="text" name="username"/>
                </td>
            </tr>
            <tr>
                <td>
                    Password:
                </td>
                <td>
                    <input type="Password" name="password"/>
                </td>
            </tr>
        </table>
        <p style="color: red">${errorMessage}</p>
        <input type="submit" value="Login">
        <a href="/register">Yet not registered?</a>
    </form>
</div>
</body>
</html>
